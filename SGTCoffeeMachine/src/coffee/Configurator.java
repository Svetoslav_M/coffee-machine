package coffee;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * 
 * @author Svetlio Configurates the GUI for coffee machine. extends JFrame.
 *
 */
public class Configurator extends JFrame {

	JFrame myFrame;
	double defaultPrice;
	JComboBox jcbCoffeeType;
	JComboBox jcbExtraShots;
	JCheckBox jDecaff;
	JTextField jCustomerName;
	JLabel jPrice;
	JPanel jNorth;

	/**
	 * constructor for the coffee machine application.
	 */
	public Configurator() {
		super("Coffee Machine");

		setSize(640, 480);
		setLayout(new BorderLayout());
		jNorth = new JPanel(new FlowLayout());
		add(jNorth, BorderLayout.NORTH);

		setDefaultCloseOperation(EXIT_ON_CLOSE);

		createWidgets();

		pack();
		setVisible(true);

	}

	/**
	 * 
	 * @author Svetlio private class Selection(Nested Class) determines the
	 *         coffee selection.
	 * @param label
	 *            is of type String and takes the type of coffee/ quantity of
	 *            extra shots.
	 * @param price
	 *            is of type double and takes the price of the coffee/price of
	 *            the extra shots.
	 */
	private class Selection {
		private String label;
		private double price;

		public Selection(String label, double price) {
			this.label = label;
			this.price = price;
		}

		public String toString() {
			return label;
		}

		public double getPrice() {
			return price;
		}
	}

	/**
	 * Creates the widgets for the coffe machine GUI.
	 * 
	 */
	public void createWidgets() {
		jcbCoffeeType = new JComboBox();
		jcbCoffeeType.addItem(new Selection("Espresso", 1.80));
		jcbCoffeeType.addItem(new Selection("Americano", 2.0));
		jcbCoffeeType.addItem(new Selection("Cappucino", 2.10));
		jcbCoffeeType.addItem(new Selection("Macchiato", 1.90));
		jcbCoffeeType.addItem(new Selection("Mocha", 2.4));
		jcbCoffeeType.addItem(new Selection("Instant", .80));

		jcbCoffeeType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				updateDisplay();
			}
		});

		jcbExtraShots = new JComboBox();
		jcbExtraShots.addItem(new Selection("No extra shots", 0));
		jcbExtraShots.addItem(new Selection("1 extra shot", .2));
		jcbExtraShots.addItem(new Selection("2 extra shots", .4));
		jcbExtraShots.addItem(new Selection("Over 9000 extra shots", 7.5));

		jcbExtraShots.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				updateDisplay();
			}
		});

		jDecaff = new JCheckBox("Decaff");
		jDecaff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				updateDisplay();
			}
		});

		jCustomerName = new JTextField();

		jPrice = new JLabel("0.00");
		updateDisplay();

		jNorth.add(jcbCoffeeType);
		jNorth.add(jcbExtraShots);
		jNorth.add(jDecaff);
		jNorth.add(jCustomerName);
		jNorth.add(jPrice);

	}

	private void updateDisplay() {
		double defaultPrice = 0;

		defaultPrice += ((Selection) jcbCoffeeType.getSelectedItem())
				.getPrice();

		defaultPrice += ((Selection) jcbExtraShots.getSelectedItem())
				.getPrice();

		if (jDecaff.getSelectedObjects() != null) {
			defaultPrice += 20.0;
		}

		jPrice.setText(Double.toString(defaultPrice));
	}

	public static void main(String[] args) {
		Configurator conf = new Configurator();

	}

}
